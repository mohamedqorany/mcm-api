package com.mcm.api.model;

import java.io.Serializable;

import javax.persistence.Embedded;

import com.mcm.api.entity.InitialCheckup;

public class VisitModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	
	@Embedded
	private InitialCheckup initialCheckup;

	public long getId() {return id;}
	public void setId(long id) {this.id = id;}

	public InitialCheckup getInitialCheckup() {	return initialCheckup;}
	public void setInitialCheckup(InitialCheckup initialCheckup) {this.initialCheckup = initialCheckup;}
	
}
