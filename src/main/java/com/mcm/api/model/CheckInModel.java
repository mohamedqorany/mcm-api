package com.mcm.api.model;

import com.mcm.api.enums.WaitingListSectionsEnum;

public class CheckInModel {

	Long patient_id;
	int visistStatus;
	WaitingListSectionsEnum section;
	
	
	public Long getPatient_id() {	return patient_id;}
	public void setPatient_id(Long patient_id) {	this.patient_id = patient_id;}
	
	public int getVisistStatus() {	return visistStatus;}
	public void setVisistStatus(int visistStatus) {	this.visistStatus = visistStatus;}
	
	public WaitingListSectionsEnum getSection() {	return section;}
	public void setSection(WaitingListSectionsEnum section) {	this.section = section;}
	
}
