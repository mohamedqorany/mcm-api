package com.mcm.api.model;

import java.io.Serializable;

import javax.persistence.Embedded;

import com.mcm.api.entity.CheckupResult;

public class VisitCheckupModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long checkupID;
	private long examinationID;
	private CheckupResult checkupResult;
	private String performer;
	private String comment;
	
	
	
	
	
	
	
	
	public long getCheckupID() {
		return checkupID;
	}
	public void setCheckupID(long checkupID) {
		this.checkupID = checkupID;
	}
	public long getExaminationID() {
		return examinationID;
	}
	public void setExaminationID(long examinationID) {
		this.examinationID = examinationID;
	}
	public CheckupResult getCheckupResult() {
		return checkupResult;
	}
	public void setCheckupResult(CheckupResult checkupResult) {
		this.checkupResult = checkupResult;
	}
	public String getPerformer() {
		return performer;
	}
	public void setPerformer(String performer) {
		this.performer = performer;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
}
