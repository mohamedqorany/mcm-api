package com.mcm.api.model;

import java.io.Serializable;
import java.util.List;

public class PrescreptionModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String generalNote;
	private boolean active;
	private List<VisitMedicamentModel> visitMedicaments;
	private boolean template;
	private String templateName;
	
	public String getGeneralNote() {	return generalNote;}
	public void setGeneralNote(String generalNote) {	this.generalNote = generalNote;}
	
	public boolean isActive() {	return active;}
	public void setActive(boolean active) {	this.active = active;}
	
	public List<VisitMedicamentModel> getVisitMedicaments() {return visitMedicaments;}
	public void setVisitMedicaments(List<VisitMedicamentModel> visitMedicaments) {	this.visitMedicaments = visitMedicaments;}
	
	public long getId() {	return id;}
	public void setId(long id) {this.id = id;}
	
	public boolean isTemplate() {	return template;}
	public void setTemplate(boolean template) {	this.template = template;}
	
	public String getTemplateName() {	return templateName;}
	public void setTemplateName(String templateName) {	this.templateName = templateName;}
	
}
