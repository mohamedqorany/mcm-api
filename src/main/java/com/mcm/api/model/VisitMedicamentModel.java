package com.mcm.api.model;

public class VisitMedicamentModel {

	private long medicamentID;
	private int quantity;
	private String dosage;
	private String note;
	
	
	public long getMedicamentID() {return medicamentID;}
	public void setMedicamentID(long medicamentID) {	this.medicamentID = medicamentID;}
	public int getQuantity() {return quantity;}
	public void setQuantity(int quantity) {this.quantity = quantity;}
	public String getDosage() {	return dosage;}
	public void setDosage(String dosage) {	this.dosage = dosage;}
	public String getNote() {return note;}
	public void setNote(String note) {this.note = note;}
	
}
