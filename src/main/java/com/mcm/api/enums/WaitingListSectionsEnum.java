package com.mcm.api.enums;


public enum WaitingListSectionsEnum {
	SectionA,
	SectionB,
	SectionC,
	SectionD
}