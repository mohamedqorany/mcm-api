package com.mcm.api.enums;

public enum VisitStatusEnum {
	SYSTEM_INIT,
	INIT,
	CANCELED,
	SUSPENDED,
	STARTED,
	PRE_EXAM,
	IN_EXAM,
	POST_EXAM,
	COMPLETE
}