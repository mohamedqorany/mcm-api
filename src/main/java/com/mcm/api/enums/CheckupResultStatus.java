package com.mcm.api.enums;

public enum CheckupResultStatus {
	EMPTY, CLEAR, DANGER, CANCELED, WARN
}
