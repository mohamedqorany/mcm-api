package com.mcm.api.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.mcm.api.enums.GenderTypeEnum;

@Entity
public class Patient implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@NotNull
	private String firstName;
	@NotNull
	private String lastName;
	private String middleName;
	private String otherNames;
	@NotNull
	private Date DOB;
	@NotNull
	private GenderTypeEnum gender;
	private String address;
	private String email;
	@NotNull
	private String phoneNumber;
	private String otherContacts;
	private String observation;
	// TODO: revise the type of CAT
	private String CAT;
	private String FamilyMedicalHistory;
	private String other;
	private String photoPath;

	
	
	@OneToMany(mappedBy = "patient", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Appointment> appointments;
	
	
	
	
	
	
	
	public Patient() {}
	
	public Patient(long id) {
		this.id = id;
	}
	
	public long getId() {	return id;}
	public void setId(long id) {this.id = id;}

	public String getFirstName() {return firstName;}
	public void setFirstName(String firstName) {this.firstName = firstName;}

	public String getLastName() {return lastName;}
	public void setLastName(String lastName) {this.lastName = lastName;}

	public String getMiddleName() {return middleName;}
	public void setMiddleName(String middleName) {	this.middleName = middleName;}

	public String getOtherNames() {return otherNames;}
	public void setOtherNames(String otherNames) {	this.otherNames = otherNames;}

	public Date getDOB() {	return DOB;}
	public void setDOB(Date dOB) {DOB = dOB;}

	public GenderTypeEnum getGender() {return gender;}
	public void setGender(GenderTypeEnum gender) {this.gender = gender;}

	public String getAddress() {return address;}
	public void setAddress(String address) {this.address = address;}

	public String getEmail() {return email;}
	public void setEmail(String email) {this.email = email;}

	public String getPhoneNumber() {return phoneNumber;}
	public void setPhoneNumber(String phoneNumber) {this.phoneNumber = phoneNumber;}

	public String getOtherContacts() {return otherContacts;}
	public void setOtherContacts(String otherContacts) {	this.otherContacts = otherContacts;}

	public String getObservation() {return observation;}
	public void setObservation(String observation) {this.observation = observation;}

	public String getCAT() {return CAT;}
	public void setCAT(String cAT) {	CAT = cAT;}

	public String getPhotoPath() {return photoPath;}
	public void setPhotoPath(String photoPath) {this.photoPath = photoPath;}

	public String getFamilyMedicalHistory() {return FamilyMedicalHistory;}
	public void setFamilyMedicalHistory(String familyMedicalHistory) {FamilyMedicalHistory = familyMedicalHistory;}

	public String getOther() {	return other;}
	public void setOther(String other) {this.other = other;}
	
	public List<Appointment> getAppointments() {	return appointments;}
	public void setAppointments(List<Appointment> appointments) {this.appointments = appointments;}
	
	
}
