package com.mcm.api.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

import com.mcm.api.enums.CheckupResultStatus;

@Embeddable
public class CheckupResult implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String resultIndex;
	private String description;
	private String resultFilePaths; // the file paths will be splitted by ‘|’. Please ask me if needed.
	private CheckupResultStatus resultStatus;
	
	


	
	public String getResultIndex() {return resultIndex;}
	public void setResultIndex(String resultIndex) {this.resultIndex = resultIndex;}
	public String getResultFilePaths() {return resultFilePaths;}
	public void setResultFilePaths(String resultFilePaths) {this.resultFilePaths = resultFilePaths;}
	public String getDescription() {	return description;}
	public void setDescription(String description) {	this.description = description;	}
	public CheckupResultStatus getResultStatus() {return resultStatus;}
	public void setResultStatus(CheckupResultStatus resultStatus) {this.resultStatus = resultStatus;}
}
