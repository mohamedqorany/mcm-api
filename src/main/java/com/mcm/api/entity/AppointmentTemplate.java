package com.mcm.api.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AppointmentTemplate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String title;
	private String note;

	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}

	public String getTitle() {	return title;}
	public void setTitle(String title) {	this.title = title;}

	public String getNote() {	return note;}
	public void setNote(String note) {	this.note = note;}

}
