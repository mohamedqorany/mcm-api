package com.mcm.api.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;


@Entity
public class VisitArticle implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@OneToOne
	private Article article;
	private double quantity;
	@Transient
	private double total;
	
	
	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}

	public Article getArticle() {	return article;}
	public void setArticle(Article article) {	this.article = article;}
	
	public double getQuantity() {	return quantity;}
	public void setQuantity(double quantity) {	this.quantity = quantity;}
	
	public double getTotal() {	return total;}
	public void setTotal(double total) {	this.total = total;}

}
