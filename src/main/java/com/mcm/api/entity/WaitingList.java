package com.mcm.api.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import com.mcm.api.enums.WaitingListSectionsEnum;


@Entity
public class WaitingList implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@NotNull
	private WaitingListSectionsEnum section;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "patient_id", nullable = false, unique=true)
	private Patient patient;

	

	
	
	public WaitingList() {}
	public WaitingList(@NotNull WaitingListSectionsEnum section, Patient patient) {
		this.section = section;
		this.patient = patient;
	}
	
	
	
	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}

	public WaitingListSectionsEnum getSection() {	return section;}
	public void setSection(WaitingListSectionsEnum section) {	this.section = section;}
	
	public Patient getPatient() {	return patient;}
	public void setPatient(Patient patient) {this.patient = patient;}

}
