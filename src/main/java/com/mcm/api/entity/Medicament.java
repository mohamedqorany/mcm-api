package com.mcm.api.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Medicament implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String code;
	private String label;
	private boolean isGeneric;
	private String molecule;
	private int rank;
	
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "labo_id", nullable = true, insertable = true, updatable = true)
	private Labo labo;

	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}

	public String getCode() {	return code;}
	public void setCode(String code) {	this.code = code;}

	public String getLabel() {	return label;}
	public void setLabel(String label) {	this.label = label;}

	public boolean isGeneric() {	return isGeneric;}
	public void setGeneric(boolean isGeneric) {	this.isGeneric = isGeneric;}

	public String getMolecule() {	return molecule;}
	public void setMolecule(String molecule) {	this.molecule = molecule;}

	public int getRank() {	return rank;}
	public void setRank(int rank) {	this.rank = rank;}

	public Labo getLabo() {	return labo;}
	public void setLabo(Labo labo) {	this.labo = labo;}
	
}
