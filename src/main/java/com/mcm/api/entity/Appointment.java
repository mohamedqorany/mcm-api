package com.mcm.api.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
public class Appointment implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@NotNull
	private String title;
	private Date startDate;
	private Date endDate;
	private String note;

	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "patient_id", nullable = true)
	private Patient patient;

	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}

	public String getTitle() {	return title;}
	public void setTitle(String title) {	this.title = title;}

	public Date getStartDate() {	return startDate;}
	public void setStartDate(Date startDate) {	this.startDate = startDate;}

	public Date getEndDate() {	return endDate;}
	public void setEndDate(Date endDate) {	this.endDate = endDate;}

	public String getNote() {	return note;}
	public void setNote(String note) {	this.note = note;}

}
