package com.mcm.api.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.mcm.api.entity.lookup.PaymentStatus;


@Entity
public class Payment implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private Date date;
	private String paymentNote;
	@OneToOne
	private PaymentStatus status;


	@Transient
	private double total;
	
	@Transient
	private double balance;
	
	@Transient
	private double deposit;

	@Transient
	private String patientName;

	@OneToMany()
	@JoinTable(name = "payment_article", joinColumns = {
			@JoinColumn(name = "payment_id", referencedColumnName = "id") }, 
	inverseJoinColumns = {
					@JoinColumn(name = "article_id", referencedColumnName = "id") })
	private List<Article> articles;

	
	@OneToMany()
	@JoinTable(name = "payment_visit_article", joinColumns = {
	@JoinColumn(name = "payment_id", referencedColumnName = "id") }, 
	inverseJoinColumns = {
	@JoinColumn(name = "visit_article_id", referencedColumnName = "id") })
	private List<VisitArticle> visitArticles;

	
	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}

	public Date getDate() {	return date;}
	public void setDate(Date date) {	this.date = date;}

	public String getPaymentNote() {	return paymentNote;}
	public void setPaymentNote(String paymentNote) {this.paymentNote = paymentNote;}
	
	public PaymentStatus getStatus() {	return status;}
	public void setStatus(PaymentStatus status) {	this.status = status;}

	public double getTotal() {	return total;}
	public void setTotal(double total) {	this.total = total;}

	public String getPatientName() {return patientName;}
	public void setPatientName(String patientName) {this.patientName = patientName;}

	public List<Article> getArticles() {return articles;}
	public void setArticles(List<Article> articles) {this.articles = articles;}
	
	public double getBalance() {	return balance;}
	public void setBalance(double balance) {	this.balance = balance;}
	
	public double getDeposit() {	return deposit;}
	public void setDeposit(double deposit) {	this.deposit = deposit;}
	
	public List<VisitArticle> getVisitArticles() {	return visitArticles;}
	public void setVisitArticles(List<VisitArticle> visitArticles) {	this.visitArticles = visitArticles;}
	
}
