package com.mcm.api.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.springframework.data.rest.core.annotation.RestResource;

@Entity
public class Checkup implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String code;
	private String description;
	private Boolean isArticle;

	@OneToOne(cascade =  CascadeType.ALL)
	@JoinColumn(name = "article_id", nullable = true)
	@RestResource(path = "checkupArticle", rel="article")
	private Article article;
	

	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}

	public String getName() {	return name;}
	public void setName(String name) {	this.name = name;}

	public String getDescription() {	return description;}
	public void setDescription(String description) {	this.description = description;}
	
	public Boolean getIsArticle() {	return isArticle;}
	public void setIsArticle(Boolean isArticle) {	this.isArticle = isArticle;	}
	
	public String getCode() {	return code;}
	public void setCode(String code) {	this.code = code;}
	
	public Article getArticle() {	return article;}
	public void setArticle(Article article) {	this.article = article;}
	
}
