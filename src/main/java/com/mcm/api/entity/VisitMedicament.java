package com.mcm.api.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class VisitMedicament implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@OneToOne
	private Medicament medicament;
	private int quantity;
	private String dosage;
	private String note;
	
	
	public VisitMedicament() {}
	public VisitMedicament(Medicament medicament, int quantity, String dosage, String note) {
		super();
		this.medicament = medicament;
		this.quantity = quantity;
		this.dosage = dosage;
		this.note = note;
	}
	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}
	
	public Medicament getMedicament() {	return medicament;}
	public void setMedicament(Medicament medicament) {	this.medicament = medicament;}
	
	public int getQuantity() {	return quantity;}
	public void setQuantity(int quantity) {	this.quantity = quantity;}
	
	public String getDosage() {	return dosage;}
	public void setDosage(String dosage) {	this.dosage = dosage;}

	public String getNote() {	return note;}
	public void setNote(String note) {	this.note = note;}
	

}
