package com.mcm.api.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.mcm.api.enums.VisitStatusEnum;

@Entity
public class Visit implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private Date date;
	private VisitStatusEnum status;
	
	
	@OneToOne(fetch = FetchType.LAZY, cascade =  CascadeType.ALL)
	@JoinColumn(name = "payment_id")
	private Payment payment;
	
	@OneToOne(fetch = FetchType.LAZY, cascade =  CascadeType.ALL)
	@JoinColumn(name = "examination_id")
	private Examination examination;
	
	@OneToOne(fetch = FetchType.LAZY, cascade =  CascadeType.ALL)
	@JoinColumn(name = "prescription_id")
	private Prescription prescription;
	
	@OneToOne(fetch = FetchType.LAZY, cascade =  CascadeType.ALL)
	@JoinColumn(name = "patient_id")
	private Patient patient;

	
	
	public long getId() {		return id;	}
	public void setId(long id) {	this.id = id;}

	public Date getDate() {	return date;}
	public void setDate(Date date) {	this.date = date;	}

	public Payment getPayment() {	return payment;}
	public void setPayment(Payment payment) {	this.payment = payment;}
	
	public Examination getExamination() {	return examination;}
	public void setExamination(Examination examination) {	this.examination = examination;}

	public Prescription getPrescription() {	return prescription;}
	public void setPrescription(Prescription prescription) {	this.prescription = prescription;}

	public Patient getPatient() {	return patient;}
	public void setPatient(Patient patient) {this.patient = patient;	}
	
	public VisitStatusEnum getStatus() {	return status;}
	public void setStatus(VisitStatusEnum status) {this.status = status;}
	
}
