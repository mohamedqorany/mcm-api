package com.mcm.api.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Prescription implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String generalNote;

	@OneToMany(fetch = FetchType.LAZY)
	private List<VisitMedicament> visitMedicaments;

	private boolean active;
	private boolean template;
	
	
	public Prescription() {}
	public Prescription(boolean active) {
		super();
		this.active = active;
	}
	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}
	
	public List<VisitMedicament> getVisitMedicaments() {return visitMedicaments;}
	public void setVisitMedicaments(List<VisitMedicament> visitMedicaments) {this.visitMedicaments = visitMedicaments;}

	public String getGeneralNote() {	return generalNote;}
	public void setGeneralNote(String generalNote) {this.generalNote = generalNote;}

	public boolean isActive() {	return active;	}
	public void setActive(boolean active) {	this.active = active;}
	
	public boolean isTemplate() {	return template;}
	public void setTemplate(boolean template) {	this.template = template;}

}
