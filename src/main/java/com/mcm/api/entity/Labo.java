package com.mcm.api.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Labo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String code;
	private String name;
	private String representative;
	private String delegate;
	private String contact;
	private String country;
	private int rank;

	
	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}

	public String getCode() {	return code;}
	public void setCode(String code) {this.code = code;}

	public String getName() {return name;}
	public void setName(String name) {this.name = name;}

	public String getRepresentative() {return representative;}
	public void setRepresentative(String representative) {this.representative = representative;}

	public String getDelegate() {return delegate;}
	public void setDelegate(String delegate) {this.delegate = delegate;}

	public String getContact() {return contact;}
	public void setContact(String contact) {this.contact = contact;}

	public String getCountry() {	return country;}
	public void setCountry(String country) {this.country = country;}

	public int getRank() {return rank;}
	public void setRank(int rank) {this.rank = rank;}

}
