package com.mcm.api.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PrescriptionTemplate implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	
	@OneToOne
	private Prescription prescription;

	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}

	public String getName() {	return name;}
	public void setName(String name) {	this.name = name;}

	public Prescription getPrescription() {	return prescription;}
	public void setPrescription(Prescription prescription) {this.prescription = prescription;}
	
}
