package com.mcm.api.entity.projection;

import org.springframework.data.rest.core.config.Projection;

import com.mcm.api.entity.Checkup;

@Projection(name = "checkupInfo", types = { Checkup.class })
public interface CheckupProjection {
	long getId();
	String getName();
	String getCode();
}
