package com.mcm.api.entity.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import com.mcm.api.entity.Patient;

@Projection(name = "patientShortList", types = { Patient.class })
public interface PatientProjection {

	@Value("#{target.id}")
	long getId();

	String getFirstName();

	String getLastName();

	String getDOB();

}