package com.mcm.api.entity.projection;

import org.springframework.data.rest.core.config.Projection;

import com.mcm.api.entity.Medicament;

@Projection(name = "medicamentName", types = { Medicament.class })
public interface MedicamentProjection {
	long getId();
	String getLabel();
}
