package com.mcm.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class InitialCheckup implements Serializable{

	private static final long serialVersionUID = 1L;
	private String description;
	private Integer weight;
	private Integer height;
	private String bloodPressure;
	private String other;
	private String  bloodSugarLevel;
	private String hypertension;
	private Integer pregnancyMonth;

	
	@Column(name = "description", updatable=true, insertable=true, nullable = true)
	public String getDescription() {return description;}
	public void setDescription(String description) {this.description = description;}
	
	@Column(name = "weight", updatable=true, insertable=true, nullable = true)
	public int getWeight() {return weight;}
	public void setWeight(int weight) {this.weight = weight;}
	
	@Column(name = "height", updatable=true, insertable=true, nullable = true)
	public int getHeight() {return height;}
	public void setHeight(int height) {	this.height = height;}
	
	@Column(name = "bloodPressure", updatable=true, insertable=true, nullable = true)
	public String getBloodPressure() {return bloodPressure;}
	public void setBloodPressure(String bloodPressure) {this.bloodPressure = bloodPressure;}
	
	@Column(name = "other", updatable=true, insertable=true, nullable = true)
	public String getOther() {return other;}
	public void setOther(String other) {	this.other = other;}

	@Column(name = "bloodSugarLevel", updatable=true, insertable=true, nullable = true)
	public String getBloodSugarLevel() {		return bloodSugarLevel;	}
	public void setBloodSugarLevel(String bloodSugarLevel) {this.bloodSugarLevel = bloodSugarLevel;}
	
	@Column(name = "hypertension", updatable=true, insertable=true, nullable = true)
	public String getHypertension() {	return hypertension;}
	public void setHypertension(String hypertension) {	this.hypertension = hypertension;}
	
	@Column(name = "pregnancyMonth", updatable=true, insertable=true, nullable = true)
	public int getPregnancyMonth() {	return pregnancyMonth;}
	public void setPregnancyMonth(int pregnancyMonth) {	this.pregnancyMonth = pregnancyMonth;}
	
}
