package com.mcm.api.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class VisitCheckup implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@OneToOne
	@JoinColumn(name = "checkup_id")
	private Checkup checkup;
	
	@ManyToOne
	Examination examination;
	
	@Embedded
	private CheckupResult checkupResult;
	
	
	private String performer; // if the checkup is isArticle, the value of performer is home, and cannot be changed. 
	
	private String comment;
	
	
	

	public VisitCheckup() {}
	public VisitCheckup(Checkup checkup, Examination examination, CheckupResult checkupResult,
			String performer, String comment) {
		super();
		this.checkup = checkup;
		this.examination = examination;
		this.checkupResult = checkupResult;
		this.performer = performer;
		this.comment = comment;
	}
	
	
	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}
	
	public Checkup getCheckup() {	return checkup;}
	public void setCheckup(Checkup checkup) {	this.checkup = checkup;}
	
	public CheckupResult getCheckupResult() {	return checkupResult;}
	public void setCheckupResult(CheckupResult checkupResult) {	this.checkupResult = checkupResult;}
	
	public String getPerformer() {	return performer;}
	public void setPerformer(String performer) {	this.performer = performer;	}
	
	public Examination getExamination() {	return examination;}
	public void setExamination(Examination examination) {	this.examination = examination;}
	
	public String getComment() {	return comment;}
	public void setComment(String comment) {	this.comment = comment;}
	
}
