package com.mcm.api.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Article implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String code;
	private String label;
	private double price;
	private String description;
	
	
	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}

	public String getCode() {	return code;}
	public void setCode(String code) {	this.code = code;}

	public String getLabel() {	return label;}
	public void setLabel(String label) {	this.label = label;}

	public double getPrice() {	return price;}
	public void setPrice(double price) {	this.price = price;}

	public String getDescription() {	return description;}
	public void setDescription(String description) {	this.description = description;}
	
	public String getName() {	return name;}
	public void setName(String name) {	this.name = name;}
	
}

