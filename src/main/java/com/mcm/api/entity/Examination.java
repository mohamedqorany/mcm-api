package com.mcm.api.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Examination implements Serializable{

	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Embedded
	private InitialCheckup initialCheckup;
	
	@OneToMany(mappedBy = "examination", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<VisitCheckup> visitCheckups;
	
	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}

	public InitialCheckup getInitialCheckup() {	return initialCheckup;}
	public void setInitialCheckup(InitialCheckup initialCheckup) {this.initialCheckup = initialCheckup;}

	public List<VisitCheckup> getVisitCheckups() {	return visitCheckups;}
	public void setVisitCheckups(List<VisitCheckup> visitCheckups) {	this.visitCheckups = visitCheckups;}

}
