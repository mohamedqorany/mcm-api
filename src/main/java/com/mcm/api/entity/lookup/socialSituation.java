package com.mcm.api.entity.lookup;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class socialSituation {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String socialStuation;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSocialStuation() {
		return socialStuation;
	}

	public void setSocialStuation(String socialStuation) {
		this.socialStuation = socialStuation;
	}

}
