package com.mcm.api.entity.lookup;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PaymentStatus implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String status;

	public long getId() {	return id;}
	public void setId(long id) {	this.id = id;}

	public String getStatus() {	return status;}
	public void setStatus(String status) {this.status = status;}

}
