package com.mcm.api.config;

import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.stereotype.Component;

import com.mcm.api.entity.Checkup;
import com.mcm.api.entity.Examination;
import com.mcm.api.entity.Medicament;
import com.mcm.api.entity.Patient;
import com.mcm.api.entity.Prescription;
import com.mcm.api.entity.Visit;

@Component
public class SpringDataRestCustomization extends RepositoryRestConfigurerAdapter {

 @Override
 public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
    config.getCorsRegistry().addMapping("/**")
    .allowedOrigins("*")
    .allowedMethods("*")
    .maxAge(3600);
    config.exposeIdsFor(Patient.class);
    config.exposeIdsFor(Prescription.class);
    config.exposeIdsFor(Medicament.class);
    config.exposeIdsFor(Examination.class);
    config.exposeIdsFor(Checkup.class);
    config.exposeIdsFor(Visit.class);
  }
}