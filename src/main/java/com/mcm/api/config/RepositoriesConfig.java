package com.mcm.api.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@Configuration
public class RepositoriesConfig extends RepositoryRestConfigurerAdapter {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
    	config.setBasePath("/mcm")
    	.setDefaultPageSize(100);
        config.getCorsRegistry()
        .addMapping("/**") 
        .allowedOrigins("*") //
        .allowedMethods("OPTIONS", "HEAD", "GET", "PUT", "POST", "DELETE", "PATCH") //
        .allowedHeaders("*") //
        .exposedHeaders("WWW-Authenticate") //
        .allowCredentials(true);
        
     }
}