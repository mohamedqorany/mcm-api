package com.mcm.api.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import com.mcm.api.entity.Examination;
import com.mcm.api.entity.Payment;
import com.mcm.api.entity.Prescription;
import com.mcm.api.entity.Visit;
import com.mcm.api.entity.WaitingList;
import com.mcm.api.enums.VisitStatusEnum;
import com.mcm.api.repository.ExaminationRepository;
import com.mcm.api.repository.PaymentRepository;
import com.mcm.api.repository.PrescriptionRepository;
import com.mcm.api.repository.VisitRepository;

@Component
@RepositoryEventHandler(WaitingList.class)
public class WaitingListEventHandler {
	 
	@Autowired VisitRepository visitRepository;
	@Autowired PrescriptionRepository prescriptionRepository;
	@Autowired ExaminationRepository examinationRepository;
	@Autowired PaymentRepository paymentRepository;
	
	@HandleAfterCreate
    public void handleAfterCreateOrSave(WaitingList wl) {
		
		//create Prescription
		Prescription p = new Prescription(true);
		p = prescriptionRepository.save(p);
					
		//create Examination
		Examination ex = new Examination();
		ex = examinationRepository.save(ex);
				
		//create payment
		Payment payment = new Payment();
		payment = paymentRepository.save(payment);
				
		// create visit
		Visit v = new Visit();
		v.setDate(new Date());
		v.setPatient(wl.getPatient());
		v.setPrescription(p);
		v.setExamination(ex);
		v.setPayment(payment);
		v.setStatus(VisitStatusEnum.SYSTEM_INIT);
		visitRepository.save(v);
		
    }
}
