package com.mcm.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mcm.api.entity.Checkup;
import com.mcm.api.entity.Examination;
import com.mcm.api.entity.Medicament;
import com.mcm.api.entity.Prescription;
import com.mcm.api.entity.PrescriptionTemplate;
import com.mcm.api.entity.Visit;
import com.mcm.api.entity.VisitCheckup;
import com.mcm.api.entity.VisitMedicament;
import com.mcm.api.model.CheckInModel;
import com.mcm.api.model.VisitModel;
import com.mcm.api.model.PrescreptionModel;
import com.mcm.api.model.VisitCheckupModel;
import com.mcm.api.model.VisitMedicamentModel;
import com.mcm.api.repository.AppointmentRepository;
import com.mcm.api.repository.CheckupRepository;
import com.mcm.api.repository.ExaminationRepository;
import com.mcm.api.repository.MedicamentRepository;
import com.mcm.api.repository.PaymentRepository;
import com.mcm.api.repository.PrescriptionRepository;
import com.mcm.api.repository.PrescriptionTemplateRepository;
import com.mcm.api.repository.VisitCheckupRepository;
import com.mcm.api.repository.VisitMedicamentRepository;
import com.mcm.api.repository.VisitRepository;
import com.mcm.api.repository.WaitingListRepository;
@CrossOrigin
@RestController
@RequestMapping( value = "mcm/")
public class CheckInController {

	@Autowired WaitingListRepository waitinListRepository;
	@Autowired VisitRepository visitRepository;
	@Autowired private JdbcTemplate jdbcTemplateObject;
	@Autowired AppointmentRepository appointmentRepository;
	@Autowired PrescriptionRepository prescriptionRepository;
	@Autowired ExaminationRepository examinationRepository;
	@Autowired PaymentRepository paymentRepository;
	@Autowired MedicamentRepository medicamentRepository;
	@Autowired VisitMedicamentRepository visitMedicamentRepository;
	@Autowired PrescriptionTemplateRepository prescriptionTemplateRepository;
	@Autowired VisitCheckupRepository visitCheckupRepository;
	@Autowired CheckupRepository checkupRepository;
	
	
/*	@CrossOrigin
	@RequestMapping( value= "check_in_patient", method =RequestMethod.POST, consumes = {"text/plain", "application/*"} )
	public WaitingList checkIn(@RequestBody CheckInModel checkInModel){
	
		WaitingList wl = new WaitingList(checkInModel.getSection(), new Patient(checkInModel.getPatient_id()));
		wl= waitinListRepository.save(wl);
		
		//create Prescription
		Prescription p  = new Prescription(true);
		p = prescriptionRepository.save(p);
			
		//create Examination
		Examination ex = new Examination();
		ex = examinationRepository.save(ex);
		
		//create payment
		Payment payment = new Payment();
		payment = paymentRepository.save(payment);
		
		// create visit
		Visit v = new Visit();
		v.setDate(new Date());
		v.setPatient(new Patient(checkInModel.getPatient_id()));
		v.setPrescription(p);
		v.setExamination(ex);
		v.setPayment(payment);
		v.setStatus(VisitStatusEnum.SYSTEM_INIT);
		visitRepository.save(v);
		
		return wl;
	}
	*/
	
	
	
	
	
	
	
	
	
	// ======================================= prescription ================================
	@CrossOrigin
	@Transactional
	@RequestMapping( value= "add_prescreption", method =RequestMethod.POST, consumes = {"text/plain", "application/*"} )
	public Prescription addPrescreption(@RequestBody PrescreptionModel prescreptionModel){
	
		Optional<Prescription> prescription = prescriptionRepository.findById(prescreptionModel.getId());
		Prescription prescriptionObject = null;
		
		if(prescription.isPresent()) {
			Optional<Medicament> medicament = null;
			VisitMedicament visitMedicament = null;
			List<VisitMedicament> visitMedicamentList = new ArrayList<>();
			PrescriptionTemplate prescriptionTemplate = null;
			prescriptionObject = prescription.get();
			
			// ======================================= add template ================================
			if(prescriptionObject.getVisitMedicaments().isEmpty() && prescreptionModel.isTemplate()){
				prescriptionTemplate = new PrescriptionTemplate();
				prescriptionTemplate.setName(prescreptionModel.getTemplateName());
				prescriptionTemplate.setPrescription(prescriptionObject);
				prescriptionTemplateRepository.save(prescriptionTemplate);
			}
			
			// ======================================= add visit medicaments ================================
			for(VisitMedicamentModel visitMedicamentModel : prescreptionModel.getVisitMedicaments()){
				medicament = medicamentRepository.findById(visitMedicamentModel.getMedicamentID());
				if(medicament.isPresent()) {
					Medicament medicamentObject = medicament.get();
					visitMedicament = new VisitMedicament(medicamentObject, visitMedicamentModel.getQuantity(), visitMedicamentModel.getDosage(), visitMedicamentModel.getNote());
					visitMedicament = visitMedicamentRepository.save(visitMedicament);
					visitMedicamentList.add(visitMedicament);
					
				} else {
				    //there is no medicament in the repo with 'id'
				}
				if(prescriptionObject != null){
					prescriptionObject.getVisitMedicaments().add(visitMedicament);
				}
			}
			prescriptionObject.setGeneralNote(prescreptionModel.getGeneralNote());
			prescriptionObject.setActive(prescreptionModel.isActive());
			prescriptionObject.setTemplate(prescreptionModel.isTemplate());
		
		}else{
			 //there is no prescription in the repo with 'id'
		}
		return prescriptionObject;
	}
	
	
	
	
	
	
	
	
	// ======================================= examination ================================
	@CrossOrigin
	@Transactional
	@RequestMapping( value= "add_visit_checkups", method =RequestMethod.POST, consumes = {"text/plain", "application/*"} )
	public List<VisitCheckupModel> addVisitCheckups(@RequestBody List<VisitCheckupModel> visitCheckups){
		Optional<Examination> ex = examinationRepository.findById(visitCheckups.get(0).getExaminationID());
		Examination examination = null;
		List<VisitCheckup> visitCheckupsList = new ArrayList<>();
		if(ex.isPresent()){
			examination =  ex.get();
			Optional<Checkup> ch;
			VisitCheckup visitCheckup;
			for(VisitCheckup vch: examination.getVisitCheckups()){
				visitCheckupRepository.delete(vch);
			}
			for(VisitCheckupModel visitCheckupModel : visitCheckups){
				ch = checkupRepository.findById(visitCheckupModel.getCheckupID());
				if(ch.isPresent()) {
					visitCheckup = new VisitCheckup(ch.get(), examination, visitCheckupModel.getCheckupResult(), visitCheckupModel.getPerformer(), visitCheckupModel.getComment());
					visitCheckup = visitCheckupRepository.save(visitCheckup);
					visitCheckupsList.add(visitCheckup);
				}else{ }
			}
			examination.setVisitCheckups(visitCheckupsList);
			examinationRepository.save(examination);
		}else{}
		return visitCheckups;
	}
	
	
	// ======================================= visit ================================
	@CrossOrigin
	@Transactional
	@RequestMapping( value= "visit_initial_checkups", method =RequestMethod.PUT, consumes = {"text/plain", "application/*"} )
	public VisitModel update_initial_checkups(@RequestBody VisitModel visitModel){
		Optional<Visit> visitOptional = visitRepository.findById(visitModel.getId());
		Visit visit = null;
		if(visitOptional.isPresent()){
			visit =  visitOptional.get();
			visit.getExamination().setInitialCheckup(visitModel.getInitialCheckup());
			visitRepository.save(visit);
		}else{}
		return visitModel;
	}
	
	
	
	
	
	
	
	

		
		
		
		
		
		
		
		

		
		
		
		
		
	
	
	
	@CrossOrigin
	@RequestMapping( value= "update_visit", method=RequestMethod.PUT, consumes = {"text/plain", "application/*"} )
	public int cancelVisit(@RequestBody CheckInModel checkInModel){
		String SQL = "UPDATE visit v SET v.status = ? WHERE v.patient_id = ?";
	    jdbcTemplateObject.update(SQL, checkInModel.getVisistStatus(), checkInModel.getPatient_id());
		return  jdbcTemplateObject.update(SQL, checkInModel.getVisistStatus(), checkInModel.getPatient_id());
	}
	
	/*@CrossOrigin
	@RequestMapping( value= "today_appointments_patients", method=RequestMethod.GET, consumes = {"text/plain", "application/*"} )
	public List<Patient> getTodayAppointmentsPatients(){
		String SQL = "SELECT * from pat where id = ?";
	    List<Patient> patients = jdbcTemplateObject.query(SQL, new RowMapper<Patient>(){
				 public Patient mapRow(ResultSet rs, int rowNum) throws SQLException {
					 Patient person = new Patient();
					 person.setId(resultSet.getLong("id"));
						person.setFirstName(resultSet.getString("first_name"));
						person.setLastName(resultSet.getString("last_name"));
						person.setAge(resultSet.getInt("age"));
						return person;
				}
		});
		return  patients;
	}*/
}
