package com.mcm.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mcm.api.entity.Checkup;
import com.mcm.api.entity.projection.CheckupProjection;


@RepositoryRestResource(collectionResourceRel = "checkup", path = "checkup", excerptProjection = CheckupProjection.class)
public interface CheckupRepository extends PagingAndSortingRepository<Checkup, Long>{

}
