package com.mcm.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mcm.api.entity.Medicament;
import com.mcm.api.entity.projection.MedicamentProjection;

@RepositoryRestResource(collectionResourceRel = "medicament", path = "medicament",excerptProjection = MedicamentProjection.class)
public interface MedicamentRepository extends PagingAndSortingRepository<Medicament, Long>  {

}
