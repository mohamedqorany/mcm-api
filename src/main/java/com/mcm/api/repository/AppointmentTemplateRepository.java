package com.mcm.api.repository;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mcm.api.entity.AppointmentTemplate;

@RepositoryRestResource(collectionResourceRel = "appointment_template", path = "appointment_template")
public interface AppointmentTemplateRepository extends PagingAndSortingRepository<AppointmentTemplate, Long> {

}