package com.mcm.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mcm.api.entity.VisitArticle;


@RepositoryRestResource(collectionResourceRel = "visit_article", path = "visit_article")
public interface VisitArticleRepository  extends PagingAndSortingRepository<VisitArticle, Long>{

}
