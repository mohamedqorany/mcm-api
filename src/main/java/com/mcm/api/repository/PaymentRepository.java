package com.mcm.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mcm.api.entity.Payment;

@RepositoryRestResource(collectionResourceRel = "payment", path = "payment")
public interface PaymentRepository extends PagingAndSortingRepository<Payment, Long> {

}