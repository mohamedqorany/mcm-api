package com.mcm.api.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mcm.api.entity.Appointment;

@RepositoryRestResource(collectionResourceRel = "appointment", path = "appointment")
public interface AppointmentRepository extends PagingAndSortingRepository<Appointment, Long> {
	
	@Query("select a from Appointment a where EXTRACT (day FROM a.startDate) = EXTRACT (day FROM CURRENT_DATE) "
			+ "and EXTRACT (year FROM CURRENT_DATE) = EXTRACT (year FROM a.startDate)")
    List<Appointment> todayAppointments();
}