package com.mcm.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mcm.api.entity.Examination;

@RepositoryRestResource(collectionResourceRel = "examination", path = "examination")
public interface ExaminationRepository extends PagingAndSortingRepository<Examination, Long>{

}
