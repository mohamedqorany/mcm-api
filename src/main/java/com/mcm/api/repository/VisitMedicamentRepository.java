package com.mcm.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mcm.api.entity.VisitMedicament;

@RepositoryRestResource(collectionResourceRel = "visit_medicament", path = "visit_medicament")
public interface VisitMedicamentRepository extends PagingAndSortingRepository<VisitMedicament, Long>{

}
