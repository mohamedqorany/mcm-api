package com.mcm.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mcm.api.entity.VisitCheckup;


@RepositoryRestResource(collectionResourceRel = "visit_checkup", path = "visit_checkup")
public interface VisitCheckupRepository extends PagingAndSortingRepository<VisitCheckup, Long>{

}
