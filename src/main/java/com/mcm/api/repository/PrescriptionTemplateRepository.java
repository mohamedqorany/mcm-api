package com.mcm.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mcm.api.entity.PrescriptionTemplate;

@RepositoryRestResource(collectionResourceRel = "prescription_template", path = "prescription_template")
public interface PrescriptionTemplateRepository extends PagingAndSortingRepository<PrescriptionTemplate, Long>{

}
