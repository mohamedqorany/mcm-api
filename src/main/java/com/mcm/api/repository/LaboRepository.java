package com.mcm.api.repository;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mcm.api.entity.Labo;

@RepositoryRestResource(collectionResourceRel = "labo", path = "labo")
public interface LaboRepository extends PagingAndSortingRepository<Labo, Long> {

}