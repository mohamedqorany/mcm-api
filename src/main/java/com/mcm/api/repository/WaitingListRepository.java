package com.mcm.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mcm.api.entity.WaitingList;

@RepositoryRestResource(collectionResourceRel = "waiting_list", path = "waiting_list")
public interface WaitingListRepository extends PagingAndSortingRepository<WaitingList, Long>{

}
