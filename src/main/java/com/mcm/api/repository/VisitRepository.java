package com.mcm.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.mcm.api.entity.Visit;

@RepositoryRestResource(collectionResourceRel = "visit", path = "visit")
public interface VisitRepository extends PagingAndSortingRepository<Visit, Long>{
	
	@Query("select v from Visit v where v.status = 2")
    List<Visit> canceledVisits();
	
	@RestResource(path = "patientVisit", rel = "patientVisit")
	@Query(value = "select * from visit v where v.patient_id = :patient_id and status not in ( 2, 8) ORDER BY ID DESC LIMIT 1", nativeQuery = true)
	public Visit findByPatient(@Param("patient_id") Integer patient_id);
}
