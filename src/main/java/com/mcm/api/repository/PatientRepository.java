package com.mcm.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.mcm.api.entity.Patient;
import com.mcm.api.entity.projection.PatientProjection;

//@PreAuthorize("hasRole('ROLE_USER')")
@RepositoryRestResource(collectionResourceRel = "patient", path = "patient",excerptProjection = PatientProjection.class)
public interface PatientRepository extends PagingAndSortingRepository<Patient, Long> {

	@RestResource(path = "freePatients", rel = "freePatients")
//	@Query(value = "select * from patient p join visit v on  p.id != v.patient_id or p.id = v.patient_id where v.status not in(0,1, 4, 5,6,7)", nativeQuery = true)
	@Query(value = "select * from patient p where p.id in ( select patient_id from visit v where status not in(0,1, 4, 5,6,7)) ||  p.id not in ( select patient_id from visit)", nativeQuery = true)
	public List<Patient> getFreePatients();
}