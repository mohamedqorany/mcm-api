package com.mcm.api.repository.lookup;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mcm.api.entity.lookup.PaymentStatus;

@RepositoryRestResource(collectionResourceRel = "payment_status", path = "payment_status")
public interface PaymentStatusRepository extends PagingAndSortingRepository<PaymentStatus, Long>{

}
